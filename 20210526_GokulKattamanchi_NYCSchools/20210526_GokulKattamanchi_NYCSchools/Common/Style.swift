//
//  Style.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

struct Style {
  struct Colors {
    static var primary: UIColor {
      return UIColor.white
    }

    static var secondary: UIColor {
      return UIColor.black
    }

    struct Blue {
      /// #BDE0EB
      static var light: UIColor {
        return UIColor(red: 189.0/255.0, green: 224.0/255.0, blue: 235.0/255.0, alpha: 1.0)
      }

      /// #72BCD4
      static var medium: UIColor {
        return UIColor(red: 114.0/255.0, green: 188.0/255.0, blue: 212.0/255.0, alpha: 1.0)
      }

      static var dark: UIColor {
        return UIColor.blue
      }
    }

    struct Gray {
      /// #E0E0E0
      static var light: UIColor {
        return UIColor(red: 224.0/255.0, green: 224.0/255.0, blue: 224.0/255.0, alpha: 1.0)
      }

      static var medium: UIColor {
        return UIColor.lightGray
      }

      static var dark: UIColor {
        return UIColor.gray
      }
    }
  }

  struct Font {
    static var regularFontFamily: UIFont {
      return UIFont.systemFont(ofSize: 15, weight: .regular)
    }

    static var semiboldFontFamily: UIFont {
      return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
  }
}
