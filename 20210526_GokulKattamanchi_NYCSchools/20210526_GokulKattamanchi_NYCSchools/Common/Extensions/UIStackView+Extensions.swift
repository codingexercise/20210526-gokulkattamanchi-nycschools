//
//  UIView+Extensions .swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

extension UIStackView {
  func pinBackground() {
    let view = UIView()
    view.backgroundColor = Style.Colors.Blue.medium
    view.layer.cornerRadius = 10.0
    view.translatesAutoresizingMaskIntoConstraints = false

    self.insertSubview(view, at: 0)

    NSLayoutConstraint.activate([
      leadingAnchor.constraint(equalTo: view.leadingAnchor),
      trailingAnchor.constraint(equalTo: view.trailingAnchor),
      topAnchor.constraint(equalTo: view.topAnchor),
      bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ])
  }
}
