//
//  String+Extensions.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

extension String {

  // MARK: - Phone format

  func formatAsPhone() -> String {
    guard !self.isEmpty else {
      return self
    }

    var cleanedPhone = cleanPhoneFormat()
    if cleanedPhone.count > 10 {
      let startIndex = cleanedPhone.index(cleanedPhone.startIndex, offsetBy: cleanedPhone.count - 10)
      let endIndex = cleanedPhone.endIndex
      let range = startIndex..<endIndex

      cleanedPhone = String(cleanedPhone[range])
    }

    if cleanedPhone.count < 7 {
      let regex = "(\\d{3})(\\d+)"
      let maskedPhoneNumber = cleanedPhone.replacingOccurrences(of: regex, with: "$1.$2", options: .regularExpression)

      return maskedPhoneNumber
    } else {
      let regex = "(\\d{3})(\\d{3})(\\d+)"
      let maskedPhoneNumber = cleanedPhone.replacingOccurrences(of: regex, with: "$1.$2.$3", options: .regularExpression)

      return maskedPhoneNumber
    }
  }

  fileprivate func cleanPhoneFormat() -> String {
    do {
      let range = NSRange(location: 0, length: self.count)
      let cleanRegex = try NSRegularExpression(pattern: "\\D", options: .caseInsensitive)

      return cleanRegex.stringByReplacingMatches(in: self, options: [], range: range, withTemplate: "")
    } catch {
      return self
    }
  }
}

