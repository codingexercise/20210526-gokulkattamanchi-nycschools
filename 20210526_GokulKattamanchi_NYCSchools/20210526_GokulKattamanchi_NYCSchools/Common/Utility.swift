//
//  Utility.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

class Utility {
  class func tel(_ phone: String) {
    guard !phone.isEmpty,
      let url = URL(string: "telprompt://\(phone)") else {
        print("Invalid phone number: \(phone)")
        return
    }

    UIApplication.shared.open(url)
  }

  class func openEmail(_ email: String) {
    guard !email.isEmpty,
      let url = URL(string: "mailto:\(email)") else {
        print("Invalid email")
        return
    }

    UIApplication.shared.open(url, options: [:], completionHandler: nil)
  }
}
