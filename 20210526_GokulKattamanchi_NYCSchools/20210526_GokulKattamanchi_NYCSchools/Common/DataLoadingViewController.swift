//
//  DataLoadingViewController.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/27/21.
//

import UIKit

class DataLoadingViewController: UIViewController {

  fileprivate var containerView = UIView()

  lazy var activityIndicator : UIActivityIndicatorView = {
    let activityIndicator = UIActivityIndicatorView(style: .large)
    activityIndicator.tintColor = Style.Colors.Gray.medium
    activityIndicator.translatesAutoresizingMaskIntoConstraints = false
    return activityIndicator
  }()

  func showLoadingView() {
    containerView = UIView(frame: view.bounds)

    self.view.addSubview(containerView)

    containerView.backgroundColor = Style.Colors.primary
    containerView.alpha = 0

    UIView.animate(withDuration: 0.25) {
      self.containerView.alpha = 0.8
    }

    containerView.addSubview(activityIndicator)

    NSLayoutConstraint.activate([
      activityIndicator.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
      activityIndicator.centerXAnchor.constraint(equalTo: containerView.centerXAnchor)
    ])

    activityIndicator.startAnimating()
  }

  func dismissLoadingView() {
    activityIndicator.stopAnimating()

    DispatchQueue.main.async {
      self.containerView.removeFromSuperview()
    }
  }
}

