//
//  Constants.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

struct Constants {
  static let listOfSchools = "List of Schools"
  static let numberOfSATTestTakers = "Number of SAT Test takers:"
  static let math = "Math"
  static let reading = "Reading"
  static let writing = "Writing"
  static let avgSatMath = "SAT average score - Math"
  static let avgSatWriting = "SAT average score - Writing"
  static let avgSatReading = "SAT average score - Reading"
  static let detailInfo = "Detail Info"
  static let overview = "Overview"
  static let academicOpportunities = "Academic Opportunities"
  static let moreDetails = "More Details"
  static let extracurricularActivities = "Extracurricular Activities"
  static let email = "Email"
  static let noData = "No Data Found"
}
