//
//  SchoolDetailInfoViewController.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit
import RxSwift
import RxCocoa

class SchoolDetailInfoViewController: DataLoadingViewController {

  // MARK: - Layout objects

  lazy var mainStack: UIStackView = {
    let stackView = UIStackView()
    stackView.distribution = .fill
    stackView.spacing = 5
    stackView.axis = .vertical
    stackView.layoutMargins = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
    stackView.isLayoutMarginsRelativeArrangement = true
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()

  lazy var nameLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.textColor = Style.Colors.Blue.dark
    label.font = Style.Font.semiboldFontFamily
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var addressLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.regularFontFamily
    label.textAlignment = .left
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var emailButton: UIButton = {
    let button = makeButton()
    button.setTitle(Constants.email, for: .normal)
    return button
  }()

  lazy var phoneButton: UIButton = {
    let button = makeButton()
    return button
  }()

  lazy var closeButton: UIButton = {
    let button = UIButton()
    button.backgroundColor = Style.Colors.Gray.light
    button.tintColor = Style.Colors.secondary
    button.setImage(UIImage(named: AppIcons.close), for: .normal)
    button.layer.masksToBounds = true
    button.clipsToBounds = true
    button.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }()

  // MARK:- Properties

  let viewModel: SchoolDetailInfoViewModel
  let tableView = UITableView()
  let disposeBag = DisposeBag()

  // MARK: - Initializers

  init(viewModel: SchoolDetailInfoViewModel) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    return nil
  }

  // MARK: - Life-Cycle methods

  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupUIBind()
  }

  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    closeButton.layer.cornerRadius = closeButton.frame.height/2
  }

  private func setupUI() {
    self.view.backgroundColor = Style.Colors.primary

    self.view.addSubview(closeButton)
    self.view.addSubview(mainStack)
    self.view.addSubview(tableView)

    tableView.backgroundColor = Style.Colors.primary
    tableView.delegate = self
    tableView.dataSource = self
    tableView.separatorStyle = .none
    tableView.register(DetailInfoCell.self,
                       forCellReuseIdentifier: DetailInfoCell.reuseIdentifier)
    tableView.translatesAutoresizingMaskIntoConstraints = false

    closeButton.addTarget(self, action: #selector(closeButtonTapped), for: .touchUpInside)

    let padding: CGFloat = 20
    NSLayoutConstraint.activate([
      closeButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: padding),
      closeButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -padding),
      closeButton.widthAnchor.constraint(equalToConstant: 32),
      closeButton.heightAnchor.constraint(equalToConstant: 32),

      mainStack.topAnchor.constraint(equalTo: closeButton.bottomAnchor),
      mainStack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
      mainStack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),

      tableView.topAnchor.constraint(equalTo: self.mainStack.bottomAnchor),
      tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
      tableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
      tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
    ])


    mainStack.addArrangedSubview(nameLabel)
    mainStack.addArrangedSubview(addressLabel)
    mainStack.addArrangedSubview(addBottomStack())

    self.emailButton.addTarget(self, action: #selector(emailTapped), for: .touchUpInside)
    self.phoneButton.addTarget(self, action: #selector(phoneTapped), for: .touchUpInside)
  }

  fileprivate func addBottomStack() -> UIStackView {
    let bottomStack = makeStackView(distribution: .fillEqually)
    bottomStack.addArrangedSubview(phoneButton)
    bottomStack.addArrangedSubview(emailButton)
    return bottomStack
  }

  fileprivate func makeButton() -> UIButton {
    let button = UIButton()
    button.backgroundColor = Style.Colors.Blue.dark
    button.titleLabel?.font = Style.Font.regularFontFamily
    button.setTitleColor(Style.Colors.primary, for: .normal)
    button.translatesAutoresizingMaskIntoConstraints = false
    return button
  }

  fileprivate func makeStackView(axis: NSLayoutConstraint.Axis = .horizontal,
                                 spacing: CGFloat = 10,
                                 distribution: UIStackView.Distribution = .fill) -> UIStackView {
    let stackView = UIStackView()
    stackView.axis = axis
    stackView.distribution = distribution
    stackView.spacing = spacing
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }

  private func setupUIBind() {
    viewModel.detailInfo.asDriver().drive(
      onNext: { [weak self] detailInfo in
        guard let self = self else { return }

        DispatchQueue.main.async {
          self.nameLabel.text = detailInfo?.schoolName ?? Constants.noData
          self.addressLabel.text = detailInfo?.location ?? Constants.noData
          let phone = detailInfo?.phoneNumber?.formatAsPhone() ?? Constants.noData
          self.phoneButton.setTitle(phone, for: .normal)
        }
      }
    )
    .disposed(by: disposeBag)

    Observable.combineLatest(
      viewModel.schoolInfo.asObservable(),
      viewModel.detailInfo.asObservable(),
      resultSelector: { ($0, $1) }
    ).asObservable().subscribe(
      onNext: { [weak self] _ in
        guard let self = self else { return }
        self.tableView.reloadData()
      }
    ).disposed(by: disposeBag)
  }

  // MARK:- Action

  @objc func emailTapped() {
    guard let email = viewModel.detailInfo.value?.schoolEmail else { return }

    // Will not open on simulator, test it on real device
    Utility.openEmail(email)
  }

  @objc func phoneTapped() {
    guard let number = viewModel.detailInfo.value?.phoneNumber else { return }

    // Will not open on simulator, test it on real device
    Utility.tel(number)
  }

  @objc func closeButtonTapped() {
    self.dismiss(animated: true, completion: nil)
  }
}

// MARK: - UITableViewDelegate

extension SchoolDetailInfoViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }

  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return Constants.detailInfo
  }
}

// MARK: - UITableViewDataSource

extension SchoolDetailInfoViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.numberOfSections()
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfRows()
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailInfoCell.reuseIdentifier, for: indexPath) as? DetailInfoCell else {
      return UITableViewCell()
    }

    let title = viewModel.constants[indexPath.row]
    let subtitle = viewModel.dataStore[indexPath.row]

    let viewModel = DetailInfoViewModel(title, subtitle)
    cell.configure(with: viewModel)
    return cell
  }
}
