//
//  SchoolDetailInfoViewModel.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation
import RxSwift
import RxCocoa

class SchoolDetailInfoViewModel {

  let schoolInfo = BehaviorRelay<SchoolInfo?>(value: nil)
  let detailInfo = BehaviorRelay<SchoolDetailInfo?>(value: nil)

  // Future improvements - build a better data structure using dictionary
  // to combine dataStore and constants.
  var dataStore: [String] {
    let array: [String?] = [schoolInfo.value?.avgSatMath,
                            schoolInfo.value?.avgSatReading,
                            schoolInfo.value?.avgSatWriting,
                            detailInfo.value?.overview,
                            detailInfo.value?.academicOpportunities1,
                            detailInfo.value?.academicOpportunities2,
                            detailInfo.value?.extracurricularActivities]
    return array.compactMap({ $0 })
  }

  var constants: [String] {
    let array: [String?] = [Constants.avgSatMath,
                            Constants.avgSatReading,
                            Constants.avgSatWriting,
                            Constants.overview,
                            Constants.academicOpportunities,
                            Constants.moreDetails,
                            Constants.extracurricularActivities]
    return array.compactMap({ $0 })
  }

  var avgSatMath: String? {
    return schoolInfo.value?.avgSatMath
  }

  var avgSatReading: String? {
    return schoolInfo.value?.avgSatReading
  }

  var avgSatWriting: String? {
    return schoolInfo.value?.avgSatWriting
  }

  var overview: String? {
    return detailInfo.value?.overview
  }

  var academicOpportunities: String? {
    return detailInfo.value?.academicOpportunities1
  }

  var moreDetails: String? {
    return detailInfo.value?.academicOpportunities2
  }

  var extracurricularActivities: String? {
    return detailInfo.value?.extracurricularActivities
  }

  // MARK: - Init

  init(_ schoolInfo: SchoolInfo,
       _ detailInfo: SchoolDetailInfo?) {
    self.schoolInfo.accept(schoolInfo)
    self.detailInfo.accept(detailInfo)
  }

  // MARK:- UIHelpers

  func numberOfSections() -> Int {
    return 1
  }

  func numberOfRows() -> Int {
    return dataStore.count
  }
}
