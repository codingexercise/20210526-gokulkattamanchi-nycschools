//
//  DetailInfoCell.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

class DetailInfoCell: UITableViewCell {

  // MARK:- Properties

  static let reuseIdentifier = String(describing: self)
  var viewModel: DetailInfoViewModel?

  // MARK:- Layout objects

  lazy var mainStack: UIStackView = {
    let stackView = UIStackView()
    stackView.distribution = .fill
    stackView.spacing = 10
    stackView.axis = .vertical
    stackView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    stackView.isLayoutMarginsRelativeArrangement = true
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()

  lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.semiboldFontFamily
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var subtitleLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.textColor = Style.Colors.Gray.dark
    label.font = Style.Font.regularFontFamily
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  // MARK:- Init

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupUI()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupUI() {
    self.contentView.backgroundColor = Style.Colors.Blue.light

    self.contentView.addSubview(mainStack)
    NSLayoutConstraint.activate([
      mainStack.topAnchor.constraint(equalTo: self.contentView.topAnchor),
      mainStack.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
      mainStack.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
      mainStack.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
    ])

    mainStack.addArrangedSubview(titleLabel)
    mainStack.addArrangedSubview(subtitleLabel)
  }

  func configure(with viewModel: DetailInfoViewModel) {
    self.viewModel = viewModel

    titleLabel.text = viewModel.title
    subtitleLabel.text = viewModel.subtitle
  }
}
