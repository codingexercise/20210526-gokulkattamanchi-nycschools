//
//  DetailInfoViewModel.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

class DetailInfoViewModel {
  let title: String
  let subtitle: String

  public init(_ title: String?,
              _ subtitle: String?) {
    self.title = title ?? Constants.noData
    self.subtitle = subtitle ?? Constants.noData
  }
}
