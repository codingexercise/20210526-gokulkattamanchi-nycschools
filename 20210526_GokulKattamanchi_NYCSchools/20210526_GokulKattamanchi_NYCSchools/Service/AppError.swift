//
//  AppError.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

enum AppError: String, Error {
  case error = "Error"
  case invalidURL = "Invalid URL"
  case responseUnsuccessful = "Response unsuccessful"
  case parseError = "Data format error"
  case dataError = "No data found"
  case unableToFetchListings = "Unable to fetch listings"
}
