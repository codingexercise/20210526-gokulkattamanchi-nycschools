//
//  ListingService.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

import RxSwift

enum ServiceResult<T> {
  case success(T)
  case failure(AppError)
}

class ListingsService {
  func getSchoolsInfo(completion: @escaping (_ result: ServiceResult<[SchoolInfo]?>) -> Void) {
    let endPoint = Endpoint(path: "/resource/f9bf-2cp4.json", queryItems: [])

    NetworkManager.shared.getData(endpoint: endPoint) { (schools: [SchoolInfo]?, error) in
      if let schools = schools {
        completion(.success(schools))
      } else if let error = error {
        completion(.failure(error))
      } else {
        let err = error ?? AppError.unableToFetchListings
        completion(.failure(err))
      }
    }
  }

  func getSchoolDetailInfo(completion: @escaping (_ result: ServiceResult<[SchoolDetailInfo]?>) -> Void) {
    let endPoint = Endpoint(path: "/resource/s3k6-pzi2.json", queryItems: [])

    NetworkManager.shared.getData(endpoint: endPoint) { (details: [SchoolDetailInfo]?, error) in
      if let details = details {
        completion(.success(details))
      } else if let error = error {
        completion(.failure(error))
      } else {
        let err = error ?? AppError.unableToFetchListings
        completion(.failure(err))
      }
    }
  }
}

// MARK: Rx

extension ListingsService: ReactiveCompatible {

}

extension Reactive where Base: ListingsService {
  func fetchSchoolsInfo() -> Observable<[SchoolInfo]?> {
    return Observable.create { observer in
      self.base.getSchoolsInfo { (result) in
        switch result {
        case .success(let schools):
          observer.onNext(schools)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }

      return Disposables.create()
    }
  }

  func fetchSchoolDetailInfo() -> Observable<[SchoolDetailInfo]?> {
    return Observable.create { observer in
      self.base.getSchoolDetailInfo { (result) in
        switch result {
        case .success(let details):
          observer.onNext(details)
          observer.onCompleted()
        case .failure(let error):
          observer.onError(error)
        }
      }

      return Disposables.create()
    }
  }
}


///SAT-Results/f9bf-2cp4
