//
//  APICOnfig.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

class APIConfig {

  /// List of keys in `API.plist`
  enum PropertyKey: String {
    case scheme
    case host
    case appToken
  }

  /// Singleton object
  static let shared = APIConfig()

  fileprivate var apiData: NSDictionary = {
    guard let path = Bundle.main.path(forResource: "API", ofType: "plist"),
          let dict = NSDictionary(contentsOfFile: path) else {
      fatalError("API.plist is required")
    }

    return dict
  }()

  fileprivate func configurationValue<T>(for key: PropertyKey) -> T? {
    return apiData[key.rawValue] as? T
  }

  var scheme: String {
    return configurationValue(for: .scheme) ?? ""
  }

  var host: String {
    return configurationValue(for: .host) ?? ""
  }

  var appToken: String {
    return configurationValue(for: .appToken) ?? ""
  }

  // MARK: - Init

  private init() {}
}
