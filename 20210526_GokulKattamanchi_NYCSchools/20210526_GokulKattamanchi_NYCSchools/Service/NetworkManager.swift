//
//  NetworkManager.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

struct Endpoint {
    var path: String
    var queryItems = [URLQueryItem]()
}

class NetworkManager {
  static let shared = NetworkManager()
  let cache = NSCache<NSString, UIImage>()

  /// Method to get data.
  /// - Parameters:
  ///   - endpoint: endpoint of type Endpoint that provides URL and query items.
  ///   - completion: completion block to return  object/ error.
  func getData<T: Codable>(endpoint: Endpoint,
                             completion: @escaping(T?, AppError?) -> Void) {
    var components = URLComponents()
    components.scheme = APIConfig.shared.scheme
    components.host = APIConfig.shared.host
    components.path = endpoint.path
    components.queryItems = endpoint.queryItems

    guard let url = components.url else {
      completion(nil, AppError.invalidURL)
      return
    }

    let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
      if let _ = error {
        completion(nil, AppError.error)
      }

      guard let response = response as? HTTPURLResponse, response.statusCode ==  200 else {
        completion(nil, AppError.responseUnsuccessful)
        return
      }

      guard let data = data else {
        completion(nil, AppError.dataError)
        return
      }

      do {
        let decoder = JSONDecoder()
        let schools = try decoder.decode(T.self, from: data)
        completion(schools, nil)
      } catch {
        completion(nil, AppError.parseError)
      }
    }

    task.resume()
  }

  /// Method to download image from a url.
  /// - Parameters:
  ///   - urlString: url to download the image.
  ///   - completion: completion block to return the image.
  func downloadImage(from urlString: String, completion: @escaping(UIImage?) -> Void) {
    let cacheKey = NSString(string: urlString)

    if let image = cache.object(forKey: cacheKey) {
      completion(image)
      return
    }

    guard let url = URL(string: urlString) else {
      completion(nil)
      return
    }

    let task = URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
      guard let self = self,
            error == nil,
            let response = response as? HTTPURLResponse, response.statusCode == 200,
            let data = data,
            let image = UIImage(data: data)
      else {
        completion(nil)
        return
      }

      self.cache.setObject(image, forKey: cacheKey)
      completion(image)
    }

    task.resume()
  }
}
