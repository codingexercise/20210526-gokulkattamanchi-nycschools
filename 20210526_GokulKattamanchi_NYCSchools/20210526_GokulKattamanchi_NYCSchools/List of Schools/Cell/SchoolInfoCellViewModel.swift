//
//  SchoolInfoCellViewModel.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

class SchoolInfoCellViewModel {
    let schoolName: String?
    let numberOfTestTakers: String?
    let avgSatReading: String?
    let avgSatMath: String?
    let avgSatWriting: String?

  public init(_ info: SchoolInfo) {
    self.schoolName = info.schoolName
    self.numberOfTestTakers = info.numberOfTestTakers
    self.avgSatReading = info.avgSatReading
    self.avgSatMath = info.avgSatMath
    self.avgSatWriting = info.avgSatWriting
  }
}
