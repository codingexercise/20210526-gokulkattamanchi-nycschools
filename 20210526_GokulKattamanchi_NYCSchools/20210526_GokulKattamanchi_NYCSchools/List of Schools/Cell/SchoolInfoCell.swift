//
//  SchoolInfoCell.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit

class SchoolInfoCell: UITableViewCell {

  // MARK:- Properties

  static let reuseIdentifier = String(describing: self)
  var viewModel: SchoolInfoCellViewModel?

  // MARK:- Layout objects

  lazy var mainStack: UIStackView = {
    let stackView = UIStackView()
    stackView.distribution = .equalSpacing
    stackView.spacing = 10
    stackView.axis = .vertical
    stackView.layoutMargins = UIEdgeInsets(top: 20, left: 10, bottom: 20, right: 10)
    stackView.isLayoutMarginsRelativeArrangement = true
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }()

  lazy var nameLabel: UILabel = {
    let label = UILabel()
    label.numberOfLines = 0
    label.lineBreakMode = .byWordWrapping
    label.textColor = Style.Colors.Blue.dark
    label.font = Style.Font.semiboldFontFamily
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var numberOfTestTakersLabel: UILabel = {
    let label = UILabel()
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.regularFontFamily
    label.textAlignment = .left
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var mathScoreLabel: UILabel = {
    let label = UILabel()
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.semiboldFontFamily
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var readingScoreLabel: UILabel = {
    let label = UILabel()
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.semiboldFontFamily
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  lazy var writingScoreLabel: UILabel = {
    let label = UILabel()
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.semiboldFontFamily
    label.textAlignment = .center
    label.translatesAutoresizingMaskIntoConstraints = false
    return label
  }()

  // MARK:- Init

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupUI()
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  private func setupUI() {
    self.contentView.backgroundColor = Style.Colors.Blue.light

    self.contentView.addSubview(mainStack)
    NSLayoutConstraint.activate([
      mainStack.topAnchor.constraint(equalTo: self.contentView.topAnchor),
      mainStack.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
      mainStack.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
    ])

    mainStack.addArrangedSubview(nameLabel)

    let testTakersStack = makeStackView(distribution: .fill)
    let textLabel = makeLabel(textColor: Style.Colors.secondary,
                              font: Style.Font.regularFontFamily,
                              text: Constants.numberOfSATTestTakers)
    textLabel.textAlignment = .left
    testTakersStack.addArrangedSubview(textLabel)
    testTakersStack.addArrangedSubview(numberOfTestTakersLabel)
    mainStack.addArrangedSubview(testTakersStack)

    let subjectsStack = makeStackView(spacing: 5,
                                      distribution: .fillEqually)
    let mathStack = makeStackView(axis: .vertical,
                                  spacing: 10,
                                  distribution: .fillEqually)
    mathStack.pinBackground()
    mathStack.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    mathStack.isLayoutMarginsRelativeArrangement = true
    mathStack.addArrangedSubview(makeLabel(text: Constants.math))
    mathStack.addArrangedSubview(mathScoreLabel)

    let readingStack = makeStackView(axis: .vertical,
                                     spacing: 10,
                                     distribution: .fillEqually)
    readingStack.pinBackground()
    readingStack.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    readingStack.isLayoutMarginsRelativeArrangement = true
    readingStack.addArrangedSubview(makeLabel(text: Constants.reading))
    readingStack.addArrangedSubview(readingScoreLabel)

    let writingStack = makeStackView(axis: .vertical,
                                     spacing: 10,
                                     distribution: .fillEqually)
    writingStack.pinBackground()
    writingStack.layoutMargins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    writingStack.isLayoutMarginsRelativeArrangement = true
    writingStack.addArrangedSubview(makeLabel(text: Constants.writing))
    writingStack.addArrangedSubview(writingScoreLabel)

    subjectsStack.addArrangedSubview(mathStack)
    subjectsStack.addArrangedSubview(readingStack)
    subjectsStack.addArrangedSubview(writingStack)

    mainStack.addArrangedSubview(subjectsStack)

    let spacerView = addSpacerView()
    self.contentView.addSubview(spacerView)

    NSLayoutConstraint.activate([
      spacerView.topAnchor.constraint(equalTo: mainStack.bottomAnchor),
      spacerView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
      spacerView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
      spacerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
    ])
  }

  func configure(with viewModel: SchoolInfoCellViewModel) {
    self.viewModel = viewModel

    nameLabel.text = viewModel.schoolName
    numberOfTestTakersLabel.text = viewModel.numberOfTestTakers
    mathScoreLabel.text = viewModel.avgSatMath
    readingScoreLabel.text = viewModel.avgSatReading
    writingScoreLabel.text = viewModel.avgSatWriting
  }

  // MARK: - UIHelpers

  fileprivate func makeStackView(axis: NSLayoutConstraint.Axis = .horizontal,
                                 spacing: CGFloat = 10,
                                 distribution: UIStackView.Distribution = .fill) -> UIStackView {
    let stackView = UIStackView()
    stackView.axis = axis
    stackView.distribution = distribution
    stackView.spacing = spacing
    stackView.translatesAutoresizingMaskIntoConstraints = false
    return stackView
  }

  fileprivate func makeLabel(textColor: UIColor = Style.Colors.secondary,
                             font: UIFont = Style.Font.regularFontFamily,
                             text: String) -> UILabel {
    let label = UILabel()
    label.textColor = Style.Colors.secondary
    label.font = Style.Font.semiboldFontFamily
    label.text = text
    label.textAlignment = .center
    return label
  }

  fileprivate func makeSeparator(_ alpha: CGFloat = 0.8) -> UIView {
    let view = UIView()
    view.backgroundColor = Style.Colors.Gray.dark
    view.alpha = alpha
    view.translatesAutoresizingMaskIntoConstraints = false

    return view
  }

  fileprivate func addSpacerView() -> UIView {
    let spacerView = UIView()
    spacerView.backgroundColor = Style.Colors.primary
    spacerView.translatesAutoresizingMaskIntoConstraints = false
    spacerView.heightAnchor.constraint(equalToConstant: 15).isActive = true

    let bottomSeparator = makeSeparator()
    spacerView.addSubview(bottomSeparator)
    bottomSeparator.topAnchor.constraint(equalTo: spacerView.topAnchor).isActive = true
    bottomSeparator.widthAnchor.constraint(equalTo: spacerView.widthAnchor).isActive = true
    bottomSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true

    return spacerView
  }
}
