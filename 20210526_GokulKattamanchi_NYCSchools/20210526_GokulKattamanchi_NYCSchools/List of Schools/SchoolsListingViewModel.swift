//
//  SchoolsListingViewModel.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation
import RxSwift
import RxCocoa

class SchoolsListingViewModel {
  let service: ListingsService

  let schools = BehaviorRelay<[SchoolInfo]?>(value: nil)
  let schoolsDetailInfo = BehaviorRelay<[SchoolDetailInfo]?>(value: nil)
  let working = BehaviorRelay<Bool>(value: false)
  let error = BehaviorRelay<Error?>(value: nil)
  let disposeBag = DisposeBag()

  // MARK: - Init

  init(_ service: ListingsService = ListingsService()) {
    self.service = service
  }

  // MARK: - Networking

  func fetchSchools() {
    _ = service.rx.fetchSchoolsInfo()
      .do(onError: { error in
        self.error.accept(error)
      }, onCompleted: {
        self.fetchSchoolDetailInfo()
      }, onSubscribe: {
        self.working.accept(true)
      }, onDispose: {
        self.working.accept(false)
      })
      .catchAndReturn([])
      .bind(to: self.schools)
      .disposed(by: disposeBag)
  }

  func fetchSchoolDetailInfo() {
    _ = service.rx.fetchSchoolDetailInfo()
      .do(onError: { error in
        self.error.accept(error)
      }, onSubscribe: {
        self.working.accept(true)
      }, onDispose: {
        self.working.accept(false)
      })
      .catchAndReturn([])
      .bind(to: self.schoolsDetailInfo)
      .disposed(by: disposeBag)
  }
}
