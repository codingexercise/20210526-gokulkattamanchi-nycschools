//
//  SchoolsListingViewController.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import UIKit
import RxSwift
import RxCocoa

class SchoolsListingViewController: DataLoadingViewController {
  enum Section {
    case main
  }

  // MARK: - Layout objects

  let tableView = UITableView()

  // MARK: - Properties

  var dataSource: UITableViewDiffableDataSource<Section, SchoolInfo>?
  let viewModel: SchoolsListingViewModel
  let disposeBag = DisposeBag()

  // MARK: - Initializers

  init(viewModel: SchoolsListingViewModel = SchoolsListingViewModel()) {
    self.viewModel = viewModel
    super.init(nibName: nil, bundle: nil)
  }

  required init?(coder: NSCoder) {
    return nil
  }

  // MARK: - Life-Cycle methods

  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupUIBind()
    configureDataSource()
    buildSnapshot()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.fetchSchools()
  }

  private func setupUI() {
    self.view.backgroundColor = Style.Colors.primary
    self.navigationItem.title = Constants.listOfSchools

    // Table view
    self.view.addSubview(tableView)
    tableView.backgroundColor = .white
    tableView.delegate = self
    tableView.separatorStyle = .none
    tableView.register(SchoolInfoCell.self,
                       forCellReuseIdentifier: SchoolInfoCell.reuseIdentifier)

    tableView.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      tableView.topAnchor.constraint(equalTo: view.topAnchor),
      tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
    ])
  }

  private func setupUIBind() {
    viewModel.schools.asDriver().drive(
      onNext: { [weak self] _ in
        guard let self = self else { return }
        self.buildSnapshot()
      }
    )
    .disposed(by: disposeBag)

    viewModel.working.asDriver().do(
      onNext: { [weak self] working in
        guard let self = self else { return }

        if working {
          self.showLoadingView()
        } else {
          self.dismissLoadingView()
        }
      }
    )
    .drive(UIApplication.shared.rx.isNetworkActivityIndicatorVisible)
    .disposed(by: disposeBag)
  }

  private func configureDataSource() {
    dataSource = UITableViewDiffableDataSource<Section, SchoolInfo>(tableView: tableView, cellProvider: { (tableView, indexPath, info) -> UITableViewCell? in
      guard let cell = tableView.dequeueReusableCell(withIdentifier: SchoolInfoCell.reuseIdentifier, for: indexPath) as? SchoolInfoCell else {
        return UITableViewCell()
      }

      let viewModel = SchoolInfoCellViewModel(info)
      cell.configure(with: viewModel)
      return cell
    })
  }

  private func buildSnapshot() {
    var snapshot = NSDiffableDataSourceSnapshot<Section, SchoolInfo>()
    snapshot.appendSections([.main])
    snapshot.appendItems(viewModel.schools.value ?? [])
    DispatchQueue.main.async {
      self.dataSource?.apply(snapshot, animatingDifferences: true)
    }
  }

  // MARK:- Navigation

  private func openSchoolDetail(_ schoolInfo: SchoolInfo) {
    let detailInfo = viewModel.schoolsDetailInfo.value?.filter({ $0.id == schoolInfo.id }).first
    let viewModel = SchoolDetailInfoViewModel(schoolInfo, detailInfo)
    let viewController = SchoolDetailInfoViewController(viewModel: viewModel)
    self.present(viewController, animated: true, completion: nil)
  }
}

// MARK: - UITableViewDelegate

extension SchoolsListingViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return UITableView.automaticDimension
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let schoolInfo = viewModel.schools.value?[indexPath.row] else { return }
    openSchoolDetail(schoolInfo)
  }
}
