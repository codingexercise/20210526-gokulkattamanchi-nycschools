//
//  SchoolInfo.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

struct SchoolInfo: Codable, Hashable {
  let id: String?
  let schoolName: String?
  let numberOfTestTakers: String?
  let avgSatReading: String?
  let avgSatMath: String?
  let avgSatWriting: String?

  public init(id: String?,
              schoolName: String?,
              numberOfTestTakers: String?,
              avgSatReading: String?,
              avgSatMath: String?,
              avgSatWriting: String?) {
    self.id = id
    self.schoolName = schoolName
    self.numberOfTestTakers = numberOfTestTakers
    self.avgSatReading = avgSatReading
    self.avgSatMath = avgSatMath
    self.avgSatWriting = avgSatWriting
  }

  public enum CodingKeys: String, CodingKey {
    case id = "dbn"
    case schoolName = "school_name"
    case numberOfTestTakers = "num_of_sat_test_takers"
    case avgSatReading = "sat_critical_reading_avg_score"
    case avgSatMath = "sat_math_avg_score"
    case avgSatWriting = "sat_writing_avg_score"
  }
}
