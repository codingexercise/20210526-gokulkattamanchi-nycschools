//
//  SchoolDetailInfo.swift
//  20210526_GokulKattamanchi_NYCSchools
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import Foundation

struct SchoolDetailInfo: Codable, Hashable {
  let id: String?
  let schoolName: String?
  let overview: String?
  let availbleSeatsFor10th: String?
  let academicOpportunities1: String?
  let academicOpportunities2: String?
  let extracurricularActivities: String?
  let attendanceRate: String?
  let location: String?
  let phoneNumber: String?
  let schoolEmail: String?
  let website: String?
  let city: String?
  let zip: String?
  let stateCode: String?

  public init(id: String?,
              schoolName: String?,
              overview: String?,
              availbleSeatsFor10th: String?,
              academicOpportunities1: String?,
              academicOpportunities2: String?,
              extracurricularActivities: String?,
              attendanceRate: String?,
              location: String?,
              phoneNumber: String?,
              schoolEmail: String?,
              website: String?,
              city: String?,
              zip: String?,
              stateCode: String?) {
    self.id = id
    self.schoolName = schoolName
    self.overview = overview
    self.availbleSeatsFor10th = availbleSeatsFor10th
    self.academicOpportunities1 = academicOpportunities1
    self.academicOpportunities2 = academicOpportunities2
    self.extracurricularActivities = extracurricularActivities
    self.attendanceRate = attendanceRate
    self.location = location
    self.phoneNumber = phoneNumber
    self.schoolEmail = schoolEmail
    self.website = website
    self.city = city
    self.zip = zip
    self.stateCode = stateCode
  }

  public enum CodingKeys: String, CodingKey {
    case id = "dbn"
    case schoolName = "school_name"
    case overview = "overview_paragraph"
    case availbleSeatsFor10th = "school_10th_seats"
    case academicOpportunities1 = "academicopportunities1"
    case academicOpportunities2 = "academicopportunities2"
    case extracurricularActivities = "extracurricular_activities"
    case attendanceRate = "attendance_rate"
    case location = "location"
    case phoneNumber = "phone_number"
    case schoolEmail = "school_email"
    case website = "website"
    case city = "city"
    case zip = "zip"
    case stateCode = "state_code"
  }
}
