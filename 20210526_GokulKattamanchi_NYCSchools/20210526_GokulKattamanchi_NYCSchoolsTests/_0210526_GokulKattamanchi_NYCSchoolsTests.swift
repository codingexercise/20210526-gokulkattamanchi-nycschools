//
//  _0210526_GokulKattamanchi_NYCSchoolsTests.swift
//  20210526_GokulKattamanchi_NYCSchoolsTests
//
//  Created by Gokul Kattamanchi on 5/26/21.
//

import XCTest
@testable import _0210526_GokulKattamanchi_NYCSchools

class _0210526_GokulKattamanchi_NYCSchoolsTests: XCTestCase {

  /// Test for phone number format
  func testPhoneFormat() {
    // GIVEN
    let detailInfo: SchoolDetailInfo = SchoolDetailInfo.mock(id: "01M245", schoolName: "Clinton School", phoneNumber: "214-524-4375", schoolEmail: "test@theschool.net")

    // WHEN
    let phone = detailInfo.phoneNumber?.formatAsPhone()

    // THEN
    XCTAssertEqual(phone, "214.524.4375")
  }

  /// Test for Asynchronous network call.
  func testFetchAllVehicleListings() throws {
    let expectation = self.expectation(description: "Wait for ListingsService")
    ListingsService().getSchoolsInfo { result in
      switch result {
      case .success(let schools):
        if let schools = schools {
          print("\(schools.count) schools fetched")
        }
      case .failure(let error):
        print("Error: \(error)")
        XCTFail()
      }

      expectation.fulfill()
    }

    wait(for: [expectation], timeout: 5)

    //wait(for: [expectation], timeout: 0) // fails as expectations won't fulfill
  }
}
