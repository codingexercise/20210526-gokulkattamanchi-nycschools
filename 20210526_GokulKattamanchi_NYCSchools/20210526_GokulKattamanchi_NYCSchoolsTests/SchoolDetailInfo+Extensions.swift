//
//  SchoolDetailInfo+Extensions.swift
//  20210526_GokulKattamanchi_NYCSchoolsTests
//
//  Created by Gokul Kattamanchi on 5/27/21.
//

import Foundation
@testable import _0210526_GokulKattamanchi_NYCSchools

extension SchoolDetailInfo {
  static func mock(id: String? = nil,
                   schoolName: String? = nil,
                   overview: String? = nil,
                   availbleSeatsFor10th: String? = nil,
                   academicOpportunities1: String? = nil,
                   academicOpportunities2: String? = nil,
                   extracurricularActivities: String? = nil,
                   attendanceRate: String? = nil,
                   location: String? = nil,
                   phoneNumber: String? = nil,
                   schoolEmail: String? = nil,
                   website: String? = nil,
                   city: String? = nil,
                   zip: String? = nil,
                   stateCode: String? = nil) -> SchoolDetailInfo {

  return SchoolDetailInfo(id: id,
                          schoolName: schoolName,
                          overview: overview,
                          availbleSeatsFor10th: availbleSeatsFor10th,
                          academicOpportunities1: academicOpportunities1,
                          academicOpportunities2: academicOpportunities2,
                          extracurricularActivities: extracurricularActivities,
                          attendanceRate: attendanceRate,
                          location: location,
                          phoneNumber: phoneNumber,
                          schoolEmail: schoolEmail,
                          website: website,
                          city: city,
                          zip: zip,
                          stateCode: stateCode)
  }
}
