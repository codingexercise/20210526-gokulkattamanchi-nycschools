# JPMC :- Coding Exercise #

A simple iOS mobile application to show a list of schools with SAT scores.  
On selection, a detail page is presented with more detailed info including SAT scores.

  * Includes sample unit tests.

### Frameworks used ###

* version supported: iOS 13
* Swift
* RxSwift
* Design pattern: MVVM
* Dependencies: CocoaPods
* API:
     - https://data.cityofnewyork.us/resource/f9bf-2cp4.json
     - https://data.cityofnewyork.us/resource/s3k6-pzi2.json

### Installation? ###

* Install CocoaPods :-
  - Go to the directory.
  - Install pods using 'pod install'.
  
### Future Improvements? ###

* Add more unit tests.
* Add more data points to the UI.
* Build better data structure in the detail info view model.

### Any questions? ###

Contact email: gokulkattamanchi@gmail.com
